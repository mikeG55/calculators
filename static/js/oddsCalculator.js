function BetCalculator(n){
    this.container = n;

    this.changeOdds = function() {
          var odds_type = $('#odds-type', this.container).val()
          if (odds_type == 2) {
            $('.odds-decimal').css('display', 'inline-block');
            $('.odds-fractional').css('display', 'none');
            $('.odds span').css('display', 'none');
            $('.odds-american').css('display', 'none');
          } else if (odds_type == 1) {
            $('.odds-decimal').css('display', 'none');
            $('.odds-fractional').css('display', 'inline-block');
            $('.odds span').css('display', 'inline-block');
            $('.odds-american').css('display', 'none');
          } else {
            $('.odds-fractional').css('display', 'none');
            $('.odds span').css('display', 'none');
            $('.odds-decimal').css('display', 'none');
            $('.odds-american').css('display', 'inline-block');
          }
    };

    this.ToggleEachWay = function() {
        if ($('#each-way', this.container).is(':checked')) {
            $('.ew').css('display', 'block');
            $('.ew-factor').css('display', 'block');
            $('.win_place select').append('<option value="P">Placed</option>')
      } else {
            $('.ew').css('display', 'none');
            $('.ew-factor').css('display', 'none');
            $('.win_place select option[value="P"]').remove()
      }
    };

    this.fractToDecimNumerator = function(n) {
        // t is the fractional numerator (in 2/11 its 2)
        var t = parseInt($(n).val())
        var i = parseInt($(n).closest(".odds").find('input[name="denominator"]').val())
        var r = t / i + 1;

        $(n).closest(".odds").find('input[name="decimal"]').val(r);
        this.CalcReturnProfit()
    };

    this.fractToDecimDenominator = function(n) {
        var t = parseInt($(n).val())
        var i = parseInt($(n).closest(".odds").find('input[name="numerator"]').val())
        var r = i / t + 1;
        $(n).closest(".odds").find(".odds-decimal").val(r);
        this.CalcReturnProfit()
    };

    this.decimToFract = function(n) {

    // (example passed in 1.75)
    // current decimal odds minus 1. (example 0.75)
    var t = $(n).val() - 1

    // create a fractional odds value from the decimal (-1) value. (example 1/1)
    var r = Math.round(t) + "/1";

    // round the decimal (-1) value (example 1)
    var e = Math.round(t);

    // abs value rounded decimal (-1) minus - decimal (-1) (example 1 - 0.75 = 0.25)
    // abs returns the absolute value of the given number (example 0.25)
    var o = Math.abs(e - t);

    var f, u;

    // attempt to loop 200 times i starts at 2 can go to max of 200
    for (i = 2; i <= 200; i++)
        if (f = Math.round(t * i) / i,
            u = Math.abs(f - t),
            u < o) {
                if (r = Math.round(t * i) + "/" + i,
                    e = f,
                    u == 0)
                        break;
                    o = u
    }
    r = r.split("/");
    $(n).closest(".odds").find('input[name="numerator"]').val(r[0]);
    $(n).closest(".odds").find('input[name="denominator"]').val(r[1]);
    this.CalcReturnProfit()
};

    this.CalcReturnProfit = function() {
        stake = parseFloat($(".bet-stake", this.container).val());

        var numerator = $('input[name=numerator]').val()
        var denominator = $('input[name=denominator]').val()

        // gets the odds type
        var odds_type = $('#odds-type', this.container).val()
        console.log("odds type " + odds_type)

        // get the win place value
        var win_place = $('.win_place select', this.container).val()
        console.log("win_place " + win_place)

        // get if the bet is each way
        var is_each_way =  $("#each-way", this.container).prop("checked")
        console.log("is_each_way " + is_each_way)
        if (is_each_way) {
            var ew_factor = $('.ew select').val()
            ew_factor = ew_factor.split("/")[1]
            console.log("ew_factor " + ew_factor)
        }

        if (win_place == "L") {
            var profit = 0 - stake
            if (is_each_way) {
                // also return the ew stake
                var profit = parseFloat(profit) - parseFloat(stake)
            }
            var returns = 0

        } else if (win_place == "V") {
            var profit = 0
            var returns = stake
            if (is_each_way) {
                // also return the ew stake
                var returns = parseFloat(returns) + parseFloat(stake)
            }

        } else if (win_place == "P") {
            // bet loses
            var profit = 0 - stake
            var returns = 0

            // each way bet wins
            if (is_each_way) {
                var ew_profit = ((numerator / denominator) / ew_factor) * stake
                var profit = parseFloat(profit) + parseFloat(ew_profit)
                var ew_returns = parseFloat(ew_profit) + parseFloat(stake)
                var returns = parseFloat(returns) + parseFloat(ew_returns)
            }

        } else {
            if (parseInt(odds_type) == 2) {
                // bet wins
                var decimal = $('input[name=decimal]').val()
                var returns =  parseFloat(stake) * decimal
                var profit = parseFloat(returns) - stake
                var ew_profit = ((decimal - 1) / ew_factor) * stake

            } else {
                // bet wins
                var profit = (numerator / denominator) * stake
                var returns = parseFloat(profit) + parseFloat(stake)
                var ew_profit = ((numerator / denominator) / ew_factor) * stake
            }
            // each way bet wins
            if (is_each_way) {
                var profit = parseFloat(profit) + parseFloat(ew_profit)
                var ew_returns = parseFloat(ew_profit) + parseFloat(stake)
                var returns = parseFloat(returns) + parseFloat(ew_returns)
            }
        }
        $('.profit span').html(profit.toFixed(2))
        $('.returns span').html(returns.toFixed(2))
    };

}

$(document).ready(function(){
    var t = $('.container'), n = new BetCalculator(t);

    // if stake removed change returns/profit
    $(".bet-stake, .odds-fractional, .odds-decimal").focus(function() {
        this.value = "";
        $(".returns span, .profit span").html("-")
    });

    $(t).on("change", "select", function() {
        n.CalcReturnProfit()
    });

    //switch odds
    $('#odds-type', t).change(function() {
        console.log("Switching odds type.")
        n.changeOdds()
    });

    $("#each-way", t).change(function() {
        n.ToggleEachWay()
        n.CalcReturnProfit()
    });

    $(".bet-stake", t).keyup(function() {
        n.CalcReturnProfit()
    });

    $('.odds-fractional').keyup(function() {
        n.CalcReturnProfit()
    });
    $('.odds-decimal').keyup(function() {
        n.CalcReturnProfit()
    });
    $(".odds-decimal", t).keyup(function() {
        n.decimToFract(this)
    });
    $('.odds-fractional[name="numerator"]', t).keyup(function() {
        n.fractToDecimNumerator(this)
    });
    $('.odds-fractional[name="denominator"]', t).keyup(function() {
        n.fractToDecimDenominator(this)
    });
});





